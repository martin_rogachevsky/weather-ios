import UIKit
import MapKit

class WeatherTableViewController: BaseViewController {
    @IBOutlet weak var tableViewOutlet: UITableView!

    @IBOutlet weak var TownItem: UITabBarItem!
    
    
    weak var fullMapController: BigMapViewController?
    
    var refreshControl: UIRefreshControl!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self,
                                 action: #selector(WeatherTableViewController.handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        tableViewOutlet.refreshControl = self.refreshControl
        
        tableViewOutlet.refreshControl?.beginRefreshing()
        updateWeather()
    }
    
    override func localize() {
        weatherModel.refresh()
        tableViewOutlet.beginUpdates()
        tableViewOutlet.reloadData()
        tableViewOutlet.endUpdates()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        weatherModel.refresh()
        tableViewOutlet.beginUpdates()
        tableViewOutlet.reloadData()
        tableViewOutlet.endUpdates()
    }
    
    func handleRefresh(_ refreshControl: UIRefreshControl) {
        updateWeather()
    }
    
    private func updateWeather() {
        weatherModel.refresh()
    }
    
    override func reloadWeather(completion: ((CityAnnotation) -> ())?) {
        tableViewOutlet.reloadData()
        tableViewOutlet.refreshControl?.endRefreshing()
    }
}

extension WeatherTableViewController:  UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let city = weatherModel.cities[indexPath.row]
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "DetailsViewController") as? DetailsViewController
        nextViewController?.city = city
        navigationController?.pushViewController(nextViewController!, animated: true)
    }
}

extension WeatherTableViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return weatherModel.cities.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "weatherCell", for: indexPath) as! TownTableViewCell
        let city = weatherModel.cities[indexPath.row]
        cell.setWeathersData(for: city)
        return cell
    }
}
