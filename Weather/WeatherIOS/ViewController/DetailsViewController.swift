
//  FullMapViewController.swift
//  Informer
//
//  Created by Admin on 24.04.18.
//  Copyright © 2018 BSUIR. All rights reserved.
//

import UIKit
import MapKit

class DetailsViewController: BaseViewController, MKMapViewDelegate {
    var city: City?

    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var coordinateLabel: UILabel!
    @IBOutlet weak var imageTown: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var townLabel: UILabel!
    @IBOutlet weak var FullMapView: MKMapView!
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation {
            return nil
        }
        
        let reuseId = "reuseid"
        var av = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId)
        if av == nil {
            let annotationCity = annotation as! CityAnnotation
            av = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            av?.canShowCallout = true
            
            let infoLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 21))
            infoLabel.text = annotationCity.temperatureSubtitle! + "\n" + annotationCity.windDirectionSubtitle! + "\n" + annotationCity.windSpeedSubtitle!
            infoLabel.numberOfLines = 0
            av!.detailCalloutAccessoryView = infoLabel;
            
            let width = NSLayoutConstraint(item: infoLabel, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.lessThanOrEqual, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 300)
            infoLabel.addConstraint(width)
            
            let height = NSLayoutConstraint(item: infoLabel, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 90)
            infoLabel.addConstraint(height)
            
            let image = UIImage(named: "icon.png")
            let imageView = UIImageView(image: image)
            imageView.tag = 42
            av?.addSubview(imageView)
            av?.canShowCallout = true
            av?.frame = imageView.frame
        }
        else {
            av?.annotation = annotation
        }
        
        return av
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        FullMapView?.delegate = self
        updateUI()
        reloadWeather { cityAnnotation in
            self.FullMapView.addAnnotation(cityAnnotation)
        }
        
        showCityOnMap(city?.id ?? 0, map: FullMapView)
    }
    
    func updateUI() {
        townLabel.text = city?.name
        let latitude = city!.location.coordinate.latitude.description
        let longitude = city!.location.coordinate.longitude.description
        coordinateLabel.text = KOLocalized(key: "latitude") + ": " + latitude + "\n" + KOLocalized(key: "longitude") + ": " + longitude
        temperatureLabel.text = city!.temperature + "ºC"
        descriptionLabel.text = city?.description ?? ""
        
        let url = URL(string: (city?.image)!)
        let data = try? Data(contentsOf: url!)
        imageTown.image = UIImage(data: data!)
        setSettings(label: townLabel, delta: 10)
        setSettings(label: coordinateLabel, delta: 3)
        setSettings(label: temperatureLabel, delta: 5)
        setSettings(label: descriptionLabel, delta: 0)
        
    }

    @IBAction func longPressOnMap(_ sender: UILongPressGestureRecognizer) {
        longPressFor(map: FullMapView, sender: sender) 
    }
    
    
    func setSettings(label: UILabel, delta: CGFloat) {
        let fontSize = Settings.fontSize + delta
        label.font = UIFont(name: label.font.fontName, size: fontSize)
        label.textColor = Settings.color
        label.sizeToFit()
    }
}

