//
//  ViewControllerTabMap.swift
//  Lab2IOS
//
//  Created by Admin on 24.04.2018.
//  Copyright © 2018 BSUIR. All rights reserved.
//

import UIKit
import MapKit

class BigMapViewController: BaseViewController, MKMapViewDelegate {
    @IBOutlet weak var map: MKMapView!
    @IBOutlet weak var MapItem: UITabBarItem!
    
    override func localize() {
        weatherModel.refresh()
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation {
            return nil
        }
        
        let reuseId = "reuseid"
        var av = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId)
        if av == nil {
            let annotationCity = annotation as! CityAnnotation
            av = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            av?.canShowCallout = true
            av?.tintColor = Settings.color;
            let infoLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 21))
            infoLabel.text = annotationCity.temperatureSubtitle! + "\n" + annotationCity.windDirectionSubtitle! + "\n" + annotationCity.windSpeedSubtitle!
            infoLabel.numberOfLines = 0
            av!.detailCalloutAccessoryView = infoLabel;
            
            let width = NSLayoutConstraint(item: infoLabel, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.lessThanOrEqual, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 300)
            infoLabel.addConstraint(width)            
            let height = NSLayoutConstraint(item: infoLabel, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 90)
            infoLabel.addConstraint(height)
            
            let image = UIImage(named: "icon.png")
            let imageView = UIImageView(image: image)
            imageView.tag = 42
            av?.addSubview(imageView)
            av?.canShowCallout = true
            av?.frame = imageView.frame
        }
        else {
            av?.annotation = annotation
        }
        
        return av
    }
    
       
    override func viewDidLoad() {
        super.viewDidLoad()
        localize()
        map?.delegate = self
        reloadWeather { cityAnnotation in
            self.map.addAnnotation(cityAnnotation)
        }
        let longPressRecognizer = UILongPressGestureRecognizer(target: self,
                                                               action: #selector(longPressOnMap(sender:)))
        map.addGestureRecognizer(longPressRecognizer)
    }

    func longPressOnMap(sender: UILongPressGestureRecognizer) {
        longPressFor(map: map, sender: sender)
    }
}
