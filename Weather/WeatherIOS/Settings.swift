//
//  Settings.swift
//  CompositeApp
//
//  Created by sun on 24.04.18.
//  Copyright © 2018 BSUIR. All rights reserved.
//
import UIKit
import Foundation

class Settings{
    static var fontSize: CGFloat = 17
    static var color = UIColor(red: 0, green: 0, blue: 0, alpha: 1)
}
