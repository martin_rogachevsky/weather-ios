//
//  TownTableViewCell.swift
//  Lab2IOS
//
//  Created by Admin on 23.04.2018.
//  Copyright © 2018 BSUIR. All rights reserved.
//

import UIKit

class TownTableViewCell: UITableViewCell {
    
    @IBOutlet weak var coordinateLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var imageTown: UIImageView!

    func setWeathersData(for city: City) {
        cityLabel.text = city.name
        temperatureLabel.text = city.temperature + "ºC"
        descriptionLabel.text = city.description
        let latitude = city.location.coordinate.latitude.description
        let longitude = city.location.coordinate.longitude.description
        coordinateLabel.text = KOLocalized(key: "latitude") + ": " + latitude + "\n" + KOLocalized(key: "longitude") + ": " + longitude
        
        setSettings(label: temperatureLabel, delta: 2)
        setSettings(label: descriptionLabel, delta: 0)
        setSettings(label: coordinateLabel, delta: 2)
        setSettings(label: cityLabel, delta: 5)
        let url = URL(string: city.image)
        let data = try? Data(contentsOf: url!)
        imageTown.image = UIImage(data: data!)
    }
    
    
    func setSettings(label: UILabel, delta: CGFloat) {
        let fontSize = Settings.fontSize + delta
        label.font = UIFont(name: label.font.fontName, size: fontSize)
        label.textColor = Settings.color
        label.sizeToFit()
    }
}
