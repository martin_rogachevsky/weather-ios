//
//  BaseViewController.swift
//  Lab2IOS
//
//  CCreated by Admin on 23.04.2018.
//  Copyright © 2018 BSUIR. All rights reserved.
//

import UIKit
import MapKit

class BaseViewController: UIViewController, WeatherReloadAsyncDelegate  {
    let weatherModel = Weather.instance
    var cityAnnotations = [CityAnnotation]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNotifications()
        weatherModel.subscribe(self)
    }
    
    
    func reloadWeather(completion: ((CityAnnotation) -> ())?) {
        
            weatherModel.cities.forEach { city in
                guard let annotation = cityAnnotations.first(where: { $0.id == city.id }) else {
                    let cityAnnotation = CityAnnotation(city)
                    cityAnnotations.append(cityAnnotation)
                    if(completion != nil)
                    {
                    completion!(cityAnnotation)
                    }
                    return
                }
                annotation.update(city)
            }
    }
    
    
    
    func longPressFor(map: MKMapView, sender: UILongPressGestureRecognizer) {
        if (sender.state == .began) {
            let locationPoint = sender.location(in: map)
            let location = map.convert(locationPoint, toCoordinateFrom: map)
            let currentLocation = CLLocation(latitude: location.latitude, longitude: location.longitude)
            
            var minDist = CLLocationDistance(Int.max)
            var nearestCity: CityAnnotation? = nil
            cityAnnotations.forEach({ city in
                let dist = currentLocation.distance(from: city.location)
                if (dist < minDist) {
                    minDist = dist
                    nearestCity = city
                }
            })
            if (nearestCity != nil) {
                showCityOnMap(nearestCity!.id!, map: map)
            }
        }
    }
    
    func showCityOnMap(_ id: Int?, map: MKMapView) {
        guard let cityIndex = cityAnnotations.index(where: { $0.id == id })  else {
            return
        }
        let city = cityAnnotations[cityIndex]
        map.selectAnnotation(city, animated: true)
        let region = MKCoordinateRegionMake(city.coordinate, MKCoordinateSpanMake(1, 1))
        map.setRegion(region, animated: true)
    }
    
    func setupNotifications() {
        let nc = NotificationCenter.default
        nc.addObserver(forName: NSLocale.currentLocaleDidChangeNotification, object: nil, queue: OperationQueue.main) {
            [weak self] notification in
            guard let `self` = self else { return }
            self.view.layoutIfNeeded()
            self.localize()
        }
    }
    public func localize() -> Void{
    }
    
    
}
