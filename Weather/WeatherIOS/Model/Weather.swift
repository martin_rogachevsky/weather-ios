import CoreLocation
import Alamofire
import SwiftyJSON

class Weather {
    var delegates = [WeatherReloadAsyncDelegate]()
    var cities: [City] = []
    var enCityNames: [String] = []
    var query = "";
    static var instance: Weather = Weather()
    
    var firstQueryPart = "https://query.yahooapis.com/v1/public/yql?q=select%20wind.direction%2C%20wind.speed%2C%20item.condition.temp%2C%20item.lat%2C%20item.long%2C%20location.city%20%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%20in%20(%22";
    var queryDelemiter = "%22%2C%20%22";
    
    var secondQueryPart = "%22))%20and%20u%3D%22c%22&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys"
    
    init(){
        self.query = GetQuery()
    }
    
    private func GetQuery() -> String{
        let townsJsonEn = parseJSON(fileName: "towns");
        for townJsonEn in townsJsonEn {
            let name = (townJsonEn)["name"] as! String
            enCityNames.append(name)
        }
        let query = createQuery(jsonCityNames: enCityNames)
        return query
    }
    
    private func createQuery(jsonCityNames: [String]) -> String {
        var result = firstQueryPart
        for name in jsonCityNames {
            result += name
            if(jsonCityNames.last! != name){
                result += queryDelemiter
            }
        }
        result += secondQueryPart
        return result
    }
    
    public func subscribe(_ delegate: WeatherReloadAsyncDelegate) {
        self.delegates.append(delegate)
    }
    
    func parseJSON(fileName: String) -> [AnyObject] {
        var arrayTown: [AnyObject] = []
        let file = Bundle.main.url(forResource: fileName, withExtension: "json")
        let allCitiesData = try? Data(contentsOf: file!)
        let allCities = try? JSONSerialization.jsonObject(with: allCitiesData!, options: JSONSerialization.ReadingOptions.allowFragments) as AnyObject
        (allCities!["towns"] as? [AnyObject])?.forEach({ (city) in
            arrayTown.append(city)
        })
        return arrayTown
    }
    
    private func getCitiesFromResponse(responseJson: JSON){
        
        var fileName: String
        if  NSLocale.preferredLanguages[0] == "ru" {
            fileName = "towns_ru"
        } else {
            fileName = "towns"
        }
        var citiesJson = parseJSON(fileName: fileName)
        var id = 0
        for (_, subJson):(String, JSON) in responseJson {
            if let temperature = subJson["item"]["condition"]["temp"].string,
                let lat = Double(subJson["item"]["lat"].string!),
                let long = Double(subJson["item"]["long"].string!) {
                let location = CLLocation(latitude: lat, longitude: long)
                let cityJson = citiesJson[id] as AnyObject?
                let newWeatherItem = City(
                    id: id,
                    name: ((cityJson)!["name"] as? String)!,
                    temperature: temperature,
                    location: location,
                    description: ((cityJson)!["description"] as? String)!,
                    image: ((cityJson)!["image"] as? String)!,
                    windSpeed: subJson["wind"]["speed"].string!,
                    windDirection: subJson["wind"]["direction"].string!
                )
                self.cities.append(newWeatherItem)
                self.invokeReloadWeather()
            } else {
                print("Invalid response format")
                break
            }
            id += 1
        }
    }
    
    public func refresh() {
        Alamofire.request(query, encoding: URLEncoding.queryString).validate().responseJSON { response in
            switch response.result {
            case .success(let value):
                self.cities.removeAll()
                let cities = JSON(value)["query"]["results"]["channel"]
                self.getCitiesFromResponse(responseJson: cities)
            case .failure:
                print("Can't load weather info")
            }
        }
    }
    
    private func invokeReloadWeather() {
        delegates.forEach { delegate in
            delegate.reloadWeather(completion: nil)
        }
    }
}

protocol WeatherReloadAsyncDelegate {
    func reloadWeather(completion: ((CityAnnotation) -> ())?)
}
