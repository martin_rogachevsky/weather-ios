//
//  City.swift
//  Lab2IOS
//
//  Created by Admin on 23.04.2018.
//  Copyright © 2017 BSUIR. All rights reserved.
//

import CoreLocation

class City {
    var id: Int
    var name: String
    var temperature: String
    var windSpeed: String
    var windDirection: String
    var location: CLLocation
    var description: String
    var image: String
    
    init(id: Int, name: String, temperature: String, location: CLLocation, description: String, image: String, windSpeed: String, windDirection: String) {
        self.id = id
        self.name = name
        self.temperature = temperature
        self.location = location
        self.description = description
        self.image = image
        self.windSpeed = windSpeed
        self.windDirection = windDirection
    }
}
