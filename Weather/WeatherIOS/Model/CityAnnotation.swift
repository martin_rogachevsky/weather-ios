//
//  CityAnnotation.swift
//  Lab2IOS
//
//  Created by Admin on 23.04.2018.
//  Copyright © 2018 BSUIR. All rights reserved.
//

import MapKit

class CityAnnotation: NSObject, MKAnnotation {
    var id: Int?
    var title: String?
    var temperatureSubtitle: String?
    var windSpeedSubtitle: String?
    var windDirectionSubtitle: String?
    var latitude: Double
    var longitude: Double
    
    var coordinate: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
    
    var location: CLLocation {
        return CLLocation(latitude: latitude, longitude: longitude)
    }
    
    init(_ city: City) {
        let temperatureString = KOLocalized(key: "temperature")
        let windSpeedString = KOLocalized(key: "windSpeed")
        let windDirectionString = KOLocalized(key: "windDirection")
        let unitString = KOLocalized(key: "unit")
        self.id = city.id
        self.title = city.name
        self.temperatureSubtitle = temperatureString + ":" + city.temperature + "ºC"
        self.windDirectionSubtitle = windDirectionString + ":" + city.windDirection + "º"
        self.windSpeedSubtitle = windSpeedString + ":" + city.windSpeed + unitString
        self.latitude = city.location.coordinate.latitude
        self.longitude = city.location.coordinate.longitude
    }
        
    public func update(_ city: City) {
        if (city.id == self.id) {
            self.title = city.name
            let temperatureString = KOLocalized(key: "temperature")
            let windSpeedString = KOLocalized(key: "windSpeed")
            let windDirectionString = KOLocalized(key: "windDirection")
            let unitString = KOLocalized(key: "unit")
            self.temperatureSubtitle = temperatureString + ":" + city.temperature + "ºC"
            self.windDirectionSubtitle = windDirectionString + ":" + city.windDirection + "º"
            self.windSpeedSubtitle = windSpeedString + ":" + city.windSpeed + unitString
        }
    }
}
