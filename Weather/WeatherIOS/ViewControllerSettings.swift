//
//  ViewController.swift
//  CompositeApp
//
//  Created by Admin on 25.04.18.
//  Copyright © 2018 BSUIR. All rights reserved.
//

import UIKit

class KOLocalizedClass: NSObject {
    static let instance = KOLocalizedClass()
    
    private let localeArray:Array = ["ru","en"]
    private let keyLocale: String = "kLocale"
    private let endNameFile: String = "Localizable"
    
    private var localeDictionary : NSDictionary!
    private let typeLocalizable  : String = "strings"
    private var nameFile         : String!
    
    override init() {
        super.init()
        checkFirstInit()
    }
    
    public func changeLocalized(key:String){
        UserDefaults.standard.set("\(key)_\(endNameFile)", forKey: keyLocale)
        nameFile = "\(key)_\(endNameFile)"
        updateDictionary()
    }
    
    internal func valueWith(key:String) -> String {
        var value:String
        value = localeDictionary.object(forKey: key) as? String ?? key
        return value
    }
    
    private func checkFirstInit(){
        if UserDefaults.standard.object(forKey: keyLocale) == nil{
            var langValue:String {
                var systemLocale : String = NSLocale.preferredLanguages[0]
                
                if systemLocale.characters.count > 2 {
                    let index = systemLocale.range(of: "-")?.lowerBound
                    systemLocale = systemLocale.substring(to: index!)
                }
                
                for localeString in localeArray{
                    if localeString == systemLocale{
                        systemLocale = localeString
                    }
                }
                return systemLocale == "" ? systemLocale: "en"
            }
            UserDefaults.standard.set("\(langValue)_\(endNameFile)", forKey: keyLocale)
            nameFile = "\(langValue)_\(endNameFile)"
        }else{
            nameFile = UserDefaults.standard.object(forKey: keyLocale) as! String
        }
        updateDictionary()
    }
    
    private func updateDictionary(){
        if let path =  Bundle.main.path(forResource: nameFile, ofType: typeLocalizable) {
            localeDictionary = NSDictionary(contentsOfFile: path)!
        }
    }
}

func KOLocalized(key:String)->String{
    return KOLocalizedClass.instance.valueWith(key: key)
}

class ViewControllerSettings: BaseViewController {
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var colorLabel: UILabel!
    @IBOutlet weak var sizeLabel: UILabel!
    
    @IBOutlet weak var englishButton: UIButton!
    @IBOutlet weak var russianButton: UIButton!
    @IBOutlet weak var settingsItem: UITabBarItem!
    @IBOutlet weak var blueButton: UIButton!
    @IBOutlet weak var greenButton: UIButton!
    @IBOutlet weak var blackButton: UIButton!
    
    @IBAction func onBlack(_ sender: Any) {
        Settings.color = UIColor(red: 0, green: 0, blue: 0, alpha: 1)
        changeDisplayColor()
    }
    
    @IBAction func onGreen(_ sender: Any) {
        Settings.color = UIColor(red: 0, green: 1, blue: 0, alpha: 1)
        changeDisplayColor()
    }
    
    @IBAction func onBlue(_ sender: Any) {
        
        Settings.color = UIColor(red: 0, green: 0, blue: 1, alpha: 1)
        changeDisplayColor()
    }
    
    @IBAction func onRussian(_ sender: Any) {
        changeToLanguage("ru")
    }
    
    @IBAction func onEnglish(_ sender: Any) {
        changeToLanguage("en")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        localize()
        blueButton.setTitleColor(UIColor(red: 0, green: 0, blue: 1, alpha: 1), for: .normal)
        blackButton.setTitleColor(UIColor(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        greenButton.setTitleColor(UIColor(red: 0, green: 1, blue: 0, alpha: 1), for: .normal)
        russianButton.setTitleColor(Settings.color, for: .normal)
        englishButton.setTitleColor(Settings.color, for: .normal)
    }
    
    override func localize() {
        russianButton.setTitle(KOLocalized(key: "russian"), for: .normal)
        englishButton.setTitle(KOLocalized(key: "english"), for: .normal)
        sizeLabel.text = KOLocalized(key: "size")
        colorLabel.text = KOLocalized(key: "color")
        blackButton.setTitle(KOLocalized(key: "black"), for: .normal)
        blueButton.setTitle(KOLocalized(key: "blue"), for: .normal)
        greenButton.setTitle(KOLocalized(key: "green"), for: .normal)
    }
    
    
    @IBAction func change(_ sender: AnyObject) {
        let fontSize = CGFloat(slider.value)
        Settings.fontSize = fontSize
        setFontSettings(label: colorLabel)
        setFontSettings(label: sizeLabel)
        setFontSettings(label: russianButton.titleLabel!)
        setFontSettings(label: englishButton.titleLabel!)
        setFontSettings(label: blackButton.titleLabel!)
        setFontSettings(label: blueButton.titleLabel!)
        setFontSettings(label: greenButton.titleLabel!)
    }
    
    func setFontSettings(label: UILabel) {
        label.font = UIFont(name: label.font.fontName, size: Settings.fontSize)
        label.sizeToFit()
    }
    
    func changeDisplayColor(){
        let currentColor = Settings.color
        Settings.color = currentColor
        englishButton.setTitleColor(currentColor, for: .normal)
        russianButton.setTitleColor(currentColor, for: .normal)
        colorLabel.textColor = currentColor
        sizeLabel.textColor = currentColor
    }
    
    func changeColors(){
        changeDisplayColor()
    }
    
    
    private func changeToLanguage(_ langCode: String ){
        KOLocalizedClass.instance.changeLocalized(key: langCode)
        UserDefaults.standard.set([langCode], forKey:"AppleLanguages")
        UserDefaults.standard.synchronize()
        Locale.updateLanguage(code: langCode)
    }
}

final class AppLocale {
    fileprivate(set) var original: Locale
    fileprivate(set) var originalPreferredLanguages: [String]
    private init() {
        original = Locale.current
        originalPreferredLanguages = Locale.preferredLanguages
    }
    static var shared = AppLocale()
    
    
    private var localeIdentifier: String {
        var comps = NSLocale.components(fromLocaleIdentifier: original.identifier)
        
        if let languageCode = UserDefaults.languageCode {
            comps[NSLocale.Key.languageCode.rawValue] = languageCode
        }
        
        if let regionCode = UserDefaults.regionCode ?? original.regionCode {
            comps[NSLocale.Key.countryCode.rawValue] = regionCode
        }
        let identifier = NSLocale.localeIdentifier(fromComponents: comps)
        return identifier
    }
    
    class var identifier: String { return shared.localeIdentifier }
}

extension NSLocale {
    
    ///    This is used to override `current`. It uses `AppLocale.identifier`
    class var app: Locale {
        return Locale(identifier: AppLocale.identifier)
    }
    
    class var appPreferredLanguages: [String] {
        var arr = AppLocale.shared.originalPreferredLanguages
        if let languageCode = UserDefaults.languageCode, !arr.contains(languageCode) {
            arr.insert(languageCode, at: 0)
        }
        return arr
    }
    
    fileprivate static func swizzle(selector: Selector, with replacement: Selector) {
        let originalSelector = selector
        let swizzledSelector = replacement
        let originalMethod = class_getClassMethod(self, originalSelector)
        let swizzledMethod = class_getClassMethod(self, swizzledSelector)
        method_exchangeImplementations(originalMethod, swizzledMethod)
    }
}

extension Locale {
    ///    This should be set to the language you used as Base localization
    fileprivate static var fallbackLanguageCode: String { return AppLocale.shared.original.languageCode ?? "en" }
    
    ///    There is no sensible default for regionCode, as customers can be anywhere.
    ///    Thus optional String
    fileprivate static var fallbackRegionCode: String? { return AppLocale.shared.original.regionCode }
    
    
    ///    Saves chosen language and (optional) region code to UserDefaults so they can be restored on future app starts
    ///
    /// - Parameters:
    ///   - code: two-letter ISO 639-1 language code
    ///   - regionCode: two-letter ISO 3166-1 code
    fileprivate static func enforceLanguage(code: String, regionCode: String? = nil) {
        //    save this choice so it's automatically loaded on next cold start of the app
        UserDefaults.languageCode = code
        UserDefaults.regionCode = regionCode
        
        //    load translated bundle for the chosen language
        Bundle.enforceLanguage(code)
        
        //    update all cached stuff in the app
        DateFormatter.resetupCashed()
        NumberFormatter.resetupCashed()
    }
    
    
    static func updateLanguage(code: String, regionCode: String? = nil) {
        enforceLanguage(code: code, regionCode: regionCode)
        
        NotificationCenter.default.post(name: NSLocale.currentLocaleDidChangeNotification, object: Locale.current)
    }
    
    
    static func clearInAppOverrides() {
        UserDefaults.languageCode = nil
        UserDefaults.regionCode = nil
        
        Bundle.clearInAppOverrides()
        
        DateFormatter.resetupCashed()
        NumberFormatter.resetupCashed()
        
        NotificationCenter.default.post(name: NSLocale.currentLocaleDidChangeNotification, object: Locale.current)
    }
    
    static func setupInitialLanguage() {
        let _ = AppLocale.shared
        
        NSLocale.swizzle(selector: #selector(getter: NSLocale.current), with: #selector(getter: NSLocale.app))
        NSLocale.swizzle(selector: #selector(getter: NSLocale.preferredLanguages), with: #selector(getter: NSLocale.appPreferredLanguages))
        
        if let languageCode = UserDefaults.languageCode {
            let regionCode = UserDefaults.regionCode
            enforceLanguage(code: languageCode, regionCode: regionCode)
            NotificationCenter.default.post(name: NSLocale.currentLocaleDidChangeNotification, object: Locale.current)
            return
        }
    }
    
    var isRightToLeft: Bool {
        guard let languageCode = languageCode else { return false }
        return Locale.characterDirection(forLanguage: languageCode) == .rightToLeft
    }
}


extension UserDefaults {
    private enum Key : String {
        case languageCode = "LanguageCode"
        case regionCode = "RegionCode"
    }
    
    static var languageCode: String? {
        get {
            let defs = UserDefaults.standard
            return defs.string(forKey: Key.languageCode.rawValue)
        }
        set(value) {
            let defs = UserDefaults.standard
            if let value = value {
                defs.set(value, forKey: Key.languageCode.rawValue)
                return
            }
            defs.removeObject(forKey: Key.languageCode.rawValue)
        }
    }
    
    static var regionCode: String? {
        get {
            let defs = UserDefaults.standard
            return defs.string(forKey: Key.regionCode.rawValue)
        }
        set(value) {
            let defs = UserDefaults.standard
            if let value = value {
                defs.set(value, forKey: Key.regionCode.rawValue)
                return
            }
            defs.removeObject(forKey: Key.regionCode.rawValue)
        }
    }
}

public extension DispatchQueue {
    private static var onceTracker = [String]()
    
    ///    Execute the given `block` only once during app's lifecycle
    public class func once(token: String, block: () -> Void) {
        objc_sync_enter(self);
        defer {
            objc_sync_exit(self)
        }
        
        if onceTracker.contains(token) { return }
        onceTracker.append(token)
        block()
    }
}

public final class LocalizedBundle: Bundle {
    ///    Overrides system method and enforces usage of particular .lproj translation bundle
    override public func localizedString(forKey key: String, value: String?, table tableName: String?) -> String {
        if let bundle = Bundle.main.localizedBundle {
            return bundle.localizedString(forKey: key, value: value, table: tableName)
        }
        return super.localizedString(forKey: key, value: value, table: tableName)
    }
}


public extension Bundle {
    private struct AssociatedKeys {
        static var b = "LocalizedMainBundle"
    }
    
    fileprivate var localizedBundle: Bundle? {
        get {
            //    warning: Make sure this object you are fetching really exists
            return objc_getAssociatedObject(self, &AssociatedKeys.b) as? Bundle
        }
    }
    
    /// Loads the translations for the given language code.
    ///
    /// - Parameter code: two-letter ISO 639-1 language code
    public static func enforceLanguage(_ code: String) {
        guard let path = Bundle.main.path(forResource: code, ofType: "lproj") else { return }
        guard let bundle = Bundle(path: path) else { return }
        
        //    prepare translated bundle for chosen language and
        //    save it as property of the Bundle.main
        objc_setAssociatedObject(Bundle.main, &AssociatedKeys.b, bundle, .OBJC_ASSOCIATION_RETAIN)
        
        //    now override class of the main bundle (only once during the app lifetime)
        //    this way, `localizedString(forKey:value:table)` method in our subclass above will actually be called
        DispatchQueue.once(token: AssociatedKeys.b)  {
            object_setClass(Bundle.main, LocalizedBundle.self)
        }
    }
    
    
    ///    Removes the custom bundle
    public static func clearInAppOverrides() {
        objc_setAssociatedObject(Bundle.main, &AssociatedKeys.b, nil, .OBJC_ASSOCIATION_RETAIN)
    }
}


extension NumberFormatter {
    
    static let moneyFormatter: NumberFormatter = {
        let nf = NumberFormatter()
        nf.generatesDecimalNumbers = true
        nf.maximumFractionDigits = 2
        nf.minimumFractionDigits = 2
        nf.numberStyle = .decimal
        return nf
    }()
    
    static func resetupCashed() {
        moneyFormatter.locale = Locale.current
    }
}


extension DateFormatter {
    
    static let dobFormatter: DateFormatter = {
        let df = DateFormatter()
        df.dateStyle = .full
        return df
    }()
    
    static func resetupCashed() {
        dobFormatter.locale = Locale.current
    }
}
